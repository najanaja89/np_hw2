﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Windows.Threading;

namespace np_hw2_client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //TcpListener srvSocket;
        public MainWindow()
        {
            InitializeComponent();

        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {

            TcpClient client = null;

            ThreadPool.QueueUserWorkItem(ClientThreadRoutine, client);

        }

        void ClientThreadRoutine(object obj)
        {
            var client = obj as TcpClient;
            var serverIp = "";
            var port = "";
            var index = "";
            Dispatcher.Invoke(() => { serverIp = textBoxServerIp.Text;});
            Dispatcher.Invoke(() => { port = textBoxServerPort.Text; });
            Dispatcher.Invoke(() => { index=textBoxIndex.Text; }); ;
            try
            {
                client = new TcpClient(serverIp, int.Parse(port));
                NetworkStream stream = client.GetStream();

                while (true)
                {
                    // ввод сообщения
                    string message = index;
                    //message = String.Format("{0}: {1}", userName, message);
                    // преобразуем сообщение в массив байтов
                    byte[] data = Encoding.UTF8.GetBytes(message);
                    // отправка сообщения
                    stream.Write(data, 0, data.Length);

                    // получаем ответ
                    data = new byte[64]; // буфер для получаемых данных
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);
                    message = builder.ToString();
                    string[] words = message.Split(new char[] { ',' });
                    foreach (var word in words)
                    {
                        //textBoxResult.AppendText(word + "\n");
                        Dispatcher.Invoke(() => { textBoxResult.AppendText(word + "\n");});
                    }

                }
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            finally
            {
                client.Close();
            }

        }
    }
}
