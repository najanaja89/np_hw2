﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace np_hw2
{
    public class ClientObject
    {
        public TcpClient client;
        public ClientObject(TcpClient tcpClient)
        {
            client = tcpClient;
        }

        public void Process()
        {
            NetworkStream stream = null;
            try
            {
               
                stream = client.GetStream();
                byte[] data = new byte[64]; // буфер для получаемых данных
              
                    // получаем сообщение
                    StringBuilder builder = new StringBuilder();
                    int bytes = 0;
                    do
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        builder.Append(Encoding.UTF8.GetString(data, 0, bytes));
                    }
                    while (stream.DataAvailable);
                    string message;
                    using (var context = new DataContext())
                    {
                    var compare = builder.ToString();
                    var tempList = new List<Street>();

                    if (context.streets.Any(x => x.Index == compare))
                    {
                        var result = context.streets.Where(x => x.Index == compare).Select(y => y.Name).ToArray();
                        //tempList.AddRange();
                        message = string.Join(",", result);
                    }
                    else message = "index incorrect";
                   
                    }
                    
                    

                    Console.WriteLine(message);
                    // отправляем обратно сообщение в верхнем регистре
                    //message = message.Substring(message.IndexOf(':') + 1).Trim().ToUpper();
                    data = Encoding.Unicode.GetBytes(message);
                    stream.Write(data, 0, data.Length);
                    //context.Dispose();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (stream != null)
                    stream.Close();
                if (client != null)
                    client.Close();
            }
        }
    }
}
