﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace np_hw2
{
    class Program
    { 
        const int port = 12345;
        static TcpListener listener;
        static void Main(string[] args)
        {
            #region раскоментировать при первом запуске
            //Раскоментировать при первом запуске
            //using (var context = new DataContext())
            //{
            //    var streetsList = new List<Street>
            //   {
            //      new Street{Name="Weir Circle", Index="01000" },
            //      new Street{Name="Low Craigends", Index="02000" },
            //      new Street{Name="Cliveden Grove", Index="03000" },
            //      new Street{Name="Hilltop Moorings", Index="04000" },
            //      new Street{Name="Hazlitt Drive", Index="05000" },
            //      new Street{Name="Camborne Dale", Index="01000" },
            //      new Street{Name="Camborne Drive", Index="01000" },
            //      new Street{Name="Osprey Loke", Index="01000" },
            //      new Street{Name="Kingsdown Alley", Index="02000" },
            //      new Street{Name="Prince's Meadows", Index="02000" },
            //      new Street{Name="Stoops Lane", Index="03000" },
            //      new Street{Name="Christopher Manor", Index="05000" },
            //      new Street{Name="Bowden Villas", Index="05000" },
            //      new Street{Name="Worsley Leas", Index="05000" },
            //      new Street{Name="Palace Drove", Index="04000" },
            //      new Street{Name="Vulcan Wynd", Index="07000" },
            //      new Street{Name="Bowden Head", Index="04000" },
            //      new Street{Name="Clover Street", Index="03000" },
            //      new Street{Name="Allington Gardens", Index="04000" },
            //   };
            //    context.streets.AddRange(streetsList);
            //    context.SaveChanges();
            //}
            #endregion

            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), port);
                listener.Start();
                Console.WriteLine("Ожидание подключений...");

                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    ClientObject clientObject = new ClientObject(client);

                    // создаем новый поток для обслуживания нового клиента
                    ThreadPool.QueueUserWorkItem(ServerThreadRoutine, clientObject);
                    //Thread clientThread = new Thread(new ThreadStart(clientObject.Process));
                    //clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                if (listener != null)
                    listener.Stop();
            }
        }

        private static void ServerThreadRoutine(object state)
        {
            var client = state as ClientObject;
            client.Process();
        }
    }
}
