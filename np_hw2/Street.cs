﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace np_hw2
{
    public class Street
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Index { get; set; } = "";
        public string Name { get; set; }
    }
}
